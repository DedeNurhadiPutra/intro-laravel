<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller {
    public function form() {
        $title = 'Register';

        return view('/register', compact('title'));
    }

    public function welcome(Request $request) {
        $title    = 'Welcome';
        $firstName = $request['firstName'];
        $lastName = $request['lastName'];

        return view('/welcome', compact('title', 'firstName', 'lastName'));
    }
}
