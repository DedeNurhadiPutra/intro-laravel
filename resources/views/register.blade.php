<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$title}}</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label for="firstName">First Name: </label><br><br>
        <input type="text" name="firstName" id="firstName"><br><br>

        <label for="lastName">Last Name: </label><br><br>
        <input type="text" name="lastName" id="lastName"><br><br>

        <label for="gender">Gender : </label><br><br>
        <input type="radio" name="gender" id="gender">Male <br>
        <input type="radio" name="gender" id="gender">Female <br>
        <input type="radio" name="gender" id="gender">Other <br><br>

        <label for="nationality">Nationality : </label><br><br>
        <select name="nationality" id="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="american">American</option>
            <option value="japanese">Japanese</option>
        </select><br><br>

        <label for="language">Language Spoken : </label><br><br>
        <input type="checkbox" name="language" id="language">Bahasa Indonesia <br>
        <input type="checkbox" name="language" id="language">English <br>
        <input type="checkbox" name="language" id="language">Japan <br>
        <input type="checkbox" name="language" id="language">Other <br><br>

        <label for="bio">Bio : </label><br><br>
        <textarea name="bioa" id="bio" cols="30" rows="10"></textarea><br>
        <button type="submit">Sign Up</button><br><br>
    </form>
</body>

</html>